var logger = require('./lib/index.js').registerLogger('Logger');

describe('Error', function() {
	it('Should print message', function() {
		logger.error('This is an error message');	
	})
});

describe('Info', function() {
	it('Should print message', function() {
		logger.info('This is an info message');	
	});
})

describe('Notice', function() {
	it('Should print message', function() {
		logger.notice('This is a notice');
	});
})

describe('Warning', function() {
	it('Should print message', function() {
		logger.warn('This is a warning');
	});
})

describe('SQL Error', function() {
	it('Should print message', function() {
		logger.queryError('This is a SQL Error');
	});
})

describe('SQL Info', function() {
	it('Should print message', function() {
		logger.queryInfo('This is a SQL Info message');
	});
})

describe('SQL Notice', function() {
	it('Should print message', function() {
		logger.queryNotice('This is a SQL Notice');
	})
})

describe('SQL Warning', function() {
	it('Should print message', function() {
		logger.queryWarning('This is a SQL Warning');
	});
})