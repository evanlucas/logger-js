/**
 *	Module depends
 */
var colors = require('colors');

exports.registerLogger = function(title) {
	return new Logger(title);
}

var Logger = exports.Logger = function(title) {
	this.title = title;
	this.str = '['+title+']';
}

Logger.prototype.error = function(msg) {
	console.log(colors.red('ERROR'), colors.grey(this.str), msg);
}

Logger.prototype.info = function(msg) {
	console.log(colors.magenta('INFO'), colors.grey(this.str), msg);
}

Logger.prototype.notice = function(msg) {
	console.log(colors.cyan('NOTICE'), colors.grey(this.str), msg);
}

Logger.prototype.warn = function(msg) {
	console.log(colors.yellow('WARN'), colors.grey(this.str), msg);
}

Logger.prototype.queryError = function(msg) {
	console.log(colors.red('SQL ERROR'), colors.grey(this.str), msg);
}

Logger.prototype.queryInfo = function(msg) {
	console.log(colors.magenta('SQL INFO'), colors.grey(this.str), msg);
}

Logger.prototype.queryNotice = function(msg) {
	console.log(colors.cyan('SQL NOTICE'), colors.grey(this.str), msg);
}

Logger.prototype.queryWarning = function(msg) {
	console.log(colors.yellow('SQL WARN'), colors.grey(this.str), msg);
}

Logger.prototype.status = function(title, msg) {
	console.log(colors.yellow(title), msg);
}
